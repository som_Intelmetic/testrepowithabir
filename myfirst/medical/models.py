from django.db import models


# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=100)
    position = models.TextField()
    img = models.ImageField(upload_to='media')


class UserDetails(models.Model):
    message = models.TextField()
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, blank=False, unique=True)
    subject = models.CharField(max_length=200)
