from django.urls import path
from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('contact', views.contact, name='contact'),
    path('blog', views.blog, name='blog'),
    path('service', views.service, name='service'),
    path('register', views.register, name='register'),
]