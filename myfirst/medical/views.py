from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Person, UserDetails


# Create your views here.

def index(request):
    persons = Person.objects.all()
    return render(request, 'index.html', {'persons': persons})


def blog(request):
    return render(request, 'blog.html')


def about(request):
    return render(request, 'about.html')


def service(request):
    return render(request, 'service.html')


def register(request):
    if request.method == 'POST':
        user = UserDetails()
        user.message = request.POST.get('message')
        print(user.message)
        user.name = request.POST.get('name')
        print(user.name)
        user.email = request.POST.get('email')
        print(user.email)
        user.subject = request.POST.get('subject')
        print(user.subject)
        user.save()
        print('success')
        return redirect('index')
    else:
        return render(request, 'contact.html')
        print('user not created')


def contact(request):
    return render(request, 'contact.html')
